<?php

namespace App\Entity;

use App\Repository\WebSiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WebSiteRepository::class)
 */
class WebSite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $admin_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $favicon_url;

    /**
     * @ORM\OneToMany(targetEntity=Language::class, mappedBy="available_langs")
     */
    private $default_lang;

    /**
     * @ORM\ManyToMany(targetEntity=Language::class, inversedBy="availableInWebSites")
     */
    private $available_langs;

    public function __construct()
    {
        $this->default_lang = new ArrayCollection();
        $this->available_langs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdminEmail(): ?string
    {
        return $this->admin_email;
    }

    public function setAdminEmail(string $admin_email): self
    {
        $this->admin_email = $admin_email;

        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logo_url;
    }

    public function setLogoUrl(?string $logo_url): self
    {
        $this->logo_url = $logo_url;

        return $this;
    }

    public function getFaviconUrl(): ?string
    {
        return $this->favicon_url;
    }

    public function setFaviconUrl(?string $favicon_url): self
    {
        $this->favicon_url = $favicon_url;

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getDefaultLang(): Collection
    {
        return $this->default_lang;
    }

    public function addDefaultLang(Language $defaultLang): self
    {
        if (!$this->default_lang->contains($defaultLang)) {
            $this->default_lang[] = $defaultLang;
            $defaultLang->setAvailableLangs($this);
        }

        return $this;
    }

    public function removeDefaultLang(Language $defaultLang): self
    {
        if ($this->default_lang->removeElement($defaultLang)) {
            // set the owning side to null (unless already changed)
            if ($defaultLang->getAvailableLangs() === $this) {
                $defaultLang->setAvailableLangs(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getAvailableLangs(): Collection
    {
        return $this->available_langs;
    }

    public function addAvailableLang(Language $availableLang): self
    {
        if (!$this->available_langs->contains($availableLang)) {
            $this->available_langs[] = $availableLang;
        }

        return $this;
    }

    public function removeAvailableLang(Language $availableLang): self
    {
        $this->available_langs->removeElement($availableLang);

        return $this;
    }
}

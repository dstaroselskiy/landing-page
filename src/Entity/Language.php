<?php

namespace App\Entity;

use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LanguageRepository::class)
 */
class Language
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $url_prefix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $icon_url;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $local_code;

    /**
     * @ORM\ManyToOne(targetEntity=WebSite::class, inversedBy="default_lang")
     * @ORM\JoinColumn(nullable=false)
     */
    private $defaultInWebSites;

    /**
     * @ORM\ManyToMany(targetEntity=WebSite::class, mappedBy="available_langs")
     */
    private $availableInWebSites;

    public function __construct()
    {
        $this->availableInWebSites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrlPrefix(): ?string
    {
        return $this->url_prefix;
    }

    public function setUrlPrefix(string $url_prefix): self
    {
        $this->url_prefix = $url_prefix;

        return $this;
    }

    public function getIconUrl(): ?string
    {
        return $this->icon_url;
    }

    public function setIconUrl(string $icon_url): self
    {
        $this->icon_url = $icon_url;

        return $this;
    }

    public function getLocalCode(): ?string
    {
        return $this->local_code;
    }

    public function setLocalCode(string $local_code): self
    {
        $this->local_code = $local_code;

        return $this;
    }

    public function getDefaultInWebSites(): ?WebSite
    {
        return $this->defaultInWebSites;
    }

    public function setDefaultInWebSites(?WebSite $defaultInWebSites): self
    {
        $this->defaultInWebSites = $defaultInWebSites;

        return $this;
    }

    /**
     * @return Collection|WebSite[]
     */
    public function getAvailableInWebSites(): Collection
    {
        return $this->availableInWebSites;
    }

    public function addAvailableInWebSite(WebSite $availableInWebSite): self
    {
        if (!$this->availableInWebSites->contains($availableInWebSite)) {
            $this->availableInWebSites[] = $availableInWebSite;
            $availableInWebSite->addAvailableLang($this);
        }

        return $this;
    }

    public function removeAvailableInWebSite(WebSite $availableInWebSite): self
    {
        if ($this->availableInWebSites->removeElement($availableInWebSite)) {
            $availableInWebSite->removeAvailableLang($this);
        }

        return $this;
    }
}
